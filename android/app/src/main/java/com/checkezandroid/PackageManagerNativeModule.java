package com.checkezandroid;

import android.content.pm.PackageManager;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;

import java.util.Map;
import java.util.HashMap;

import android.util.Log;

public class PackageManagerNativeModule extends ReactContextBaseJavaModule {
    private final ReactApplicationContext reactContext;

    public PackageManagerNativeModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "PackageManagerNative";
    }

    @ReactMethod
    public void getPackageInfo(Promise promise) {
        try {
            PackageManager pm = this.reactContext.getPackageManager();

            WritableMap info = Arguments.createMap();

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA))
                info.putString("camera", "yes");

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
                info.putString("camera_any", "yes");

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS))
                info.putString("camera_autofocus", "yes");

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_EXTERNAL))
                info.putString("camera_external", "yes");

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH))
                info.putString("camera_flash", "yes");

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT))
                info.putString("camera_front", "yes");

            promise.resolve(info);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            promise.reject(null, ex.getMessage());
        }
    }
}