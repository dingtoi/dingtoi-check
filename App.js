import React, {Component} from 'react';
import {View} from 'react-native';

import { NativeRouter, Route } from "react-router-native";

import CheckDevice from "./src-android";
import Confirm from './src-android/confirm';
import Transaction from "./src-android/transaction";
import Thanks from "./src-android/thanks";

export default class App extends Component{
  constructor(props){
    super(props);
  }
  render() {
    return (
        <NativeRouter>
          <React.Fragment>
            <Route exact path="/" component={CheckDevice}/>
            <Route exact path="/confirm/:number" component={Confirm}/>
            <Route exact path="/transaction" component={Transaction}/>
            <Route exact path="/thanks/:number" component={Thanks}/>
          </React.Fragment>
        </NativeRouter>
    );
  }
}