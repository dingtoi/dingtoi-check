import * as React from 'react';
import { Text, View, StyleSheet,ImageBackground,TouchableOpacity} from 'react-native';

export default class CheckDevice extends React.Component {
  render() {
    return (
      <View>
          <ImageBackground source={require('../assets/check-device-bg.jpg')} style={{width: '100%', height: '100%'}}>
            <View style={styles.wrap}>
              <Text style={styles.title}>Swap-ez</Text>
              <Text style={styles.subtitle}>Thank you for downloading our Check-Ez App!</Text>
              <TouchableOpacity
                onPress={() => this.props.history.push('/transaction')}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>NEXT</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding:20
  },
  title:{
    textAlign: 'center',
    fontSize: 30,
    color:'#fff'
  },
  subtitle:{
    marginTop: 30,
    textAlign: 'center',
    fontSize: 22,  
     color:'#fff'
    },
  button: {
    marginTop: 30,
    width: 260,
    alignItems: 'center',
    backgroundColor: '#ff9800'
  },
  buttonText: {
    padding: 20,
    color: 'black'
  }
 
});