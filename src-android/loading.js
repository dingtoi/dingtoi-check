import * as React from 'react';
import { View, StyleSheet,Image } from 'react-native';

export default class Loading extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.cover}></View>
        <View style={styles.loading}>  
           <Image source={require('../assets/close.png')} style={{width: 25, height: 25}} />
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    position:'relative'
  },
  cover: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.20)',
    zIndex: 8888
  },
  loading:{
    zIndex:9999,
    position: 'absolute',
    top: '50%',
    left: '50%'
  }
});